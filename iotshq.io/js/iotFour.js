$(document).ready(function($) 
{  
  
  // Set the Xively API key (https://xively.com/users/Jrydarowski/keys)  
  xively.setKey( "bFWJZhnQezM26SvGc4gjsdooHyBt5asn4z6USAWvP5zVM5nR" );  
  
  // Replace with your own values  
  var feedID4        = 332873785,          // Feed ID  
      datastreamID5  = "Temp";       // Datastream ID  
      selector5      = "#myelementwar";   // Your element on the page  
  
  // Get datastream data from Xively  
  xively.datastream.get (feedID4, datastreamID5, function ( datastream ) {  
    // WARNING: This code is only executed when we get a response back from Xively,   
    // it will likely execute after the rest your script  
    //  
    // NOTE: The variable "datastream" will contain all the Datastream information   
    // as an object. The structure of Datastream objects can be found at:   
    // https://xively.com/dev/docs/api/quick_reference/api_resource_attributes/#datastream  
  
    // Display the current value from the datastream  
    $(selector5).html( datastream["current_value"] );  
  
    // Getting realtime!   
    // The function associated with the subscribe method will be executed   
    // every time there is an update to the datastream  
    xively.datastream.subscribe( feedID4, datastreamID5, function ( event , datastream_updated ) {  
      // Display the current value from the updated datastream  
      $(selector5).html( datastream_updated["current_value"] );  
    });  
  
  });  
  
  // WARNING: Code here will continue executing while we get the datastream data from Xively,   
  // use the function associated with datastream.get to work with the data   
  // once the request is complete  
});