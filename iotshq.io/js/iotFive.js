$(document).ready(function($) 
{  
  
  // Set the Xively API key (https://xively.com/users/Jrydarowski/keys)  
  xively.setKey( "y5KrB94qc2AuQ5IaYk4h1HpH2YLJrF9fP9ABi7rpYSgiTPLF" );  
  
  // Replace with your own values  
  var feedID5        = 1800687605,          // Feed ID  
      datastreamID6  = "motion";       // Datastream ID  
      selector6      = "#myelementmotion";   // Your element on the page 
      
      
  
  // Get datastream data from Xively  
  xively.datastream.get (feedID5, datastreamID6, function ( datastream ) {  
    // WARNING: This code is only executed when we get a response back from Xively,   
    // it will likely execute after the rest your script  
    //  
    // NOTE: The variable "datastream" will contain all the Datastream information   
    // as an object. The structure of Datastream objects can be found at:   
    // https://xively.com/dev/docs/api/quick_reference/api_resource_attributes/#datastream  
  
    // Display the current value from the datastream  
    $(selector6).html( datastream["current_value"] );  
  
    // Getting realtime!   
    // The function associated with the subscribe method will be executed   
    // every time there is an update to the datastream  
    xively.datastream.subscribe( feedID5, datastreamID6, function ( event , datastream_updated ) {  
      // Display the current value from the updated datastream  
      $(selector6).html( datastream_updated["current_value"] );  
    });  

  
  });  

});